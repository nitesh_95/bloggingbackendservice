package com.photoMania;

public class LoginResponse {

	
	private String Status ;
	private String reason;
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	@Override
	public String toString() {
		return "LoginResponse [Status=" + Status + ", reason=" + reason + "]";
	}
	
}
