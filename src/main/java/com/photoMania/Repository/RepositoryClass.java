package com.photoMania.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.photoMania.entity.EntityClass;

@Repository
public interface RepositoryClass extends JpaRepository<EntityClass, Long> {

}
