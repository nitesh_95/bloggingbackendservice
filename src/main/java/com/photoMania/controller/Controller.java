package com.photoMania.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.photoMania.LoginResponse;
import com.photoMania.Service.ServiceClass;
import com.photoMania.entity.EntityClass;

@RestController
@CrossOrigin
public class Controller {
	@Autowired
	private ServiceClass serviceClass;

	@RequestMapping("/blogs/saveAll")
	public LoginResponse save(@RequestBody EntityClass entityClass) {
		LoginResponse loginResponse = new LoginResponse();
		boolean isUpdated = serviceClass.save(entityClass);
		if(isUpdated) {
			loginResponse.setReason("Success");
			loginResponse.setStatus("00");
		}else {
			loginResponse.setReason("Failed");
			loginResponse.setStatus("s01");
		}
		
		return loginResponse;
	}
	@RequestMapping("/blogs/getAll")
	public List<EntityClass> getAll(){
		return serviceClass.getAll();
	}
	
	   @RequestMapping("blogs/update")
	    public ResponseEntity<EntityClass> createOrUpdateEmployee(@RequestBody EntityClass entity)   throws RecordNotFoundException {{
		   EntityClass updated = serviceClass.update(entity);
		   
	        return new ResponseEntity<EntityClass>(updated, new HttpHeaders(), HttpStatus.OK);
	    }


	   }
}
