package com.photoMania;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BloggingBackendServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BloggingBackendServiceApplication.class, args);
	}

}
