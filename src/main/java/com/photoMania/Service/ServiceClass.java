package com.photoMania.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.photoMania.Repository.RepositoryClass;
import com.photoMania.entity.EntityClass;

@Service
public class ServiceClass {

	@Autowired
	private RepositoryClass repositoryClass;
	
	public boolean save(EntityClass entityClass) {
		int count = 0;
		 repositoryClass.save(entityClass);
		 if(count == 0) {
			 return true;
		 }else {
			 
		return false;
		 }
	
	}
	
	public List<EntityClass> getAll(){
		return repositoryClass.findAll();
	}
	

	public EntityClass update( EntityClass entityClass) {
		Optional<EntityClass> entity = repositoryClass.findById(entityClass.getId());
		if(entity.isPresent()) {
			EntityClass newEntity = entity.get();
			newEntity.setAuthor(entityClass.getAuthor());
			newEntity.setDate(entityClass.getDate());
			newEntity.setPost(entityClass.getPost());
			newEntity.setTitle(entityClass.getTitle());
			newEntity.setPostId(entityClass.getPostId());
	
            newEntity = repositoryClass.save(newEntity);
             
           return newEntity;
		}else {
			 return entityClass;
		}
		
	}
}
